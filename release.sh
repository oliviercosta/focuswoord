#!/bin/sh


IMAGE_ID=$(docker inspect ${HEROKU_REGISTRY_IMAGE} --format={{.Id}})
#IMAGE_ID=sha256:714040fb0befbc8bf14f2d34ea2114f626d1a76974361245ff6b8b6500eb988c
#HEROKU_AUTH_TOKEN=
#HEROKU_APP_NAME=pacific-reaches-85087

#more info : https://devcenter.heroku.com/articles/platform-api-reference#formation-batch-update

PAYLOAD='{"updates": [{"type": "web", "docker_image": "'"$IMAGE_ID"'"}]}'

echo "----------------"
echo "payload:" 
echo $PAYLOAD
echo "----------------"
echo "HEROKU_APP_NAME"
echo $HEROKU_APP_NAME
echo "----------------"

curl --http1.1 -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME/formation \
  -d "${PAYLOAD}" \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer ${HEROKU_AUTH_TOKEN}"
